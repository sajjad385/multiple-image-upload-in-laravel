<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
})->name('home');


Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('settings', 'ProfileController@index')->name('settings');
    Route::resource('category', 'CategoryController');
    Route::resource('income', 'IncomeController');
    Route::resource('expense', 'ExpenseController');
    Route::put('profile-update', 'ProfileController@updateProfile')->name('profile.update');
    Route::put('password-update', 'ProfileController@updatePassword')->name('password.update');


});

Route::get('/form/all', 'FormController@index')->name('multiple.index');
Route::get('form','FormController@create')->name('multiple.create');
Route::post('form','FormController@store')->name('multiple.store');
Route::delete('/form/{form}', 'FormController@delete')->name('multiple.destroy');
Route::get('/{form}/form', 'FormController@edit')->name('multiple.edit');
Route::put('/form/{form}', 'FormController@update')->name('multiple.update');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
