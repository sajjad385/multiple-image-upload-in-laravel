
@extends('layouts.app')
@section('content')
    <div class="container">

        <h3 class="jumbotron">Laravel Multiple File Upload</h3>
        <form method="post" action="{{route('multiple.update',$form->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" value="{{$form->title}}" placeholder="Enter Your Title" class="form-control">
            </div>
            <br>
            <div class="input-group control-group increment">
                <input type="file" name="filename[]" class="form-control" hidden>
                <div class="input-group-btn">
                    <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                </div>
            </div>
            <div class="clone hide">
                <div class="control-group input-group" style="margin-top:10px">
                    @php
                        $allImage = json_decode($form->filename)
                    @endphp

                    @foreach($allImage as $img)
                        @if (asset('uploads/images/'.$img))
                            <img src="{{ asset('uploads/images/'.$img) }}" width="80" height="80">
                        @else
                            <p>No image found</p>
                        @endif
                    <input type="file" name="filename[]"  value="{{ asset('uploads/images/'.$img) }}" class="form-control">
                    @endforeach
                    <div class="input-group-btn">
                        <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary" style="margin-top:10px">Submit</button>

        </form>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $(".btn-success").click(function(){
                var html = $(".clone").html();
                $(".increment").after(html);
            });

            $("body").on("click",".btn-danger",function(){
                $(this).parents(".control-group").remove();
            });

        });
        </script>
@endsection