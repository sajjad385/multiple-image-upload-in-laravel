@extends('layouts.app')
@section('content')
    <div class="container">
        <a href="{{route('multiple.create')}}" class="btn btn-success mb-3">Add IMAGE</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sl</th>
                <th>Title</th>
                <th>IMAGE</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($multiple as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->title}}</td>
                    <td>
                        @if($category->filename)
                            @php
                                $allImage = json_decode($category->filename)
                            @endphp
                            @foreach($allImage as $img)
                            <img src="{{asset('uploads/images/'.$img)}}" width="80" height="80" alt="User"/>
                            @endforeach
                        @endif
                    </td>
                    <td>
                        <a   class="btn btn-primary"  href="{{route('multiple.edit',$category->id)}}">Edit</a>
                        <form  action="{{route('multiple.destroy',$category->id)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('delete')
                            <button  class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection