@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('settings')}}">Settings</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <!-- User Info -->
                        <div class="user-info">
                            <div class="image">
                                <img src="{{asset('uploads/profile/'.Auth::user()->image)}}" width="80" height="80" alt="User"/>
                            </div>
                            <div class="info-container">
                                <div class="name" data-toggle="dropdown" aria-haspopup="true"
                                     aria-expanded="false">Name : {{Auth::user()->name}}</div>
                                <div class="email">Email : {{Auth::user()->email}}</div>

                            </div>
                        </div>
                        <!-- #User Info -->

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
