<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        $multiple= Form::all();
        return view('multiple.index',compact('multiple'));
    }

    public function create()
    {
        return view('multiple.create');
    }

    public function store(Request $request)

    {
        $this->validate($request, [
            'title' => 'required',
            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($request->hasfile('filename'))
        {
            foreach($request->file('filename') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/uploads/images/', $name);
                $data[] = $name;
            }
        }
        $form= new Form();
        $form->title=$request->title;
        $form->filename=json_encode($data);
        $form->save();
        return redirect()->route('multiple.index')->with('success', 'Your images has been Added successfully');
    }
        public function edit(Form $form){

            return view('multiple.edit',compact('form'));
        }

    public function update(Request $request, $id)
    {

//        dd($request->all());
        $getid= Form::findOrFail($id);
        $this->validate($request, [
            'title' => 'required',
            'filename' => 'required',
            'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($request->hasfile('filename'))
        {
            foreach($request->file('filename') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/uploads/images/', $name);
                $data[] = $name;
            }
        }
        $getid->title=$request->title;
        $getid->filename=json_encode($data);
        $getid->save();
        return redirect()->route('multiple.index')->with('success', 'Your images has been Updated successfully');
    }
    public function delete(Form $form)
    {
        if ($form->filename) {
            $image = json_decode($form->filename);
//            dd($image);
            foreach ($image as $key=> $value){
                $image_path = public_path() .'/uploads/images/'.$value;
//                dd($image_path);
                unlink($image_path);
            }

        } else {

        }
        $form->delete();
        return back()->with('success', 'Your images has been Deleted successfully');
    }

}
